package equ.phoenix.simplejpa.dto;

import lombok.*;

@Value
public class ClientRequestDto {
    private String name;
    private String lastName;
    private String email;
    private String password;
}
