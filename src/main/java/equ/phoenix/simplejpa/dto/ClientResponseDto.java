package equ.phoenix.simplejpa.dto;

import lombok.*;

@Value
public class ClientResponseDto {
    private Long id;
    private String name;
    private String lastName;
    private String email;
}
