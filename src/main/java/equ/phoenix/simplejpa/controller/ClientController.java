package equ.phoenix.simplejpa.controller;

import equ.phoenix.simplejpa.dto.*;
import equ.phoenix.simplejpa.mapper.*;
import equ.phoenix.simplejpa.model.Client;
import equ.phoenix.simplejpa.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.*;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @GetMapping("")
    public List<ClientResponseDto> getClients() {
        var clients = clientService.getClients();

        return clients.stream()
            .map(ClientMapper::clientToDto)
            .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientResponseDto> getClientById(@PathVariable Long id) {
        var clientDto = ClientMapper.clientToDto(clientService.getClient(id));

        return ResponseEntity.ok(clientDto);
    }

    @PostMapping
    public ResponseEntity<ClientResponseDto> addClient(@RequestBody ClientRequestDto client) {
        Client newClient = clientService.addClient(ClientMapper.dtoToClient(client));
        ClientResponseDto clientResponseDto = ClientMapper.clientToDto(newClient);

        return ResponseEntity.status(HttpStatus.CREATED).body(clientResponseDto);
    }

}
