package equ.phoenix.simplejpa.service;

import equ.phoenix.simplejpa.model.Client;
import equ.phoenix.simplejpa.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {
  private final ClientRepository repository;

  public Client addClient(Client client) {
    Client currentClient = repository.getClientByEmail(client.getEmail());

    if (currentClient == null) {
        return repository.save(client);
    } else {
        throw new RuntimeException();
    }
  }

  public List<Client> getClients() {
    return (List<Client>) repository.findAll();
  }

  public Client getClient(Long id) {
    return repository.findById(id).orElse(null);
  }

  /*
  Реализуйте CRUD-операции для клиента:
    •	Создание нового клиента
    •	Получение клиента по его id
    •	Получение всех клиентов
    •	Обновление клиента
    •	Удаление клиента

    Вы НЕ должны менять что-то в репозитории.
   */
}
