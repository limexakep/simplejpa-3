package equ.phoenix.simplejpa.mapper;

import equ.phoenix.simplejpa.dto.*;
import equ.phoenix.simplejpa.model.*;

public class ClientMapper {

    public static Client dtoToClient(ClientRequestDto clientRequestDto) {
        return new Client(
            clientRequestDto.getName(),
            clientRequestDto.getLastName(),
            clientRequestDto.getEmail(),
            clientRequestDto.getPassword()
        );
    }

    public static ClientResponseDto clientToDto(Client client) {
        return new ClientResponseDto(
            client.getId(),
            client.getName(),
            client.getLastName(),
            client.getEmail()
        );
    }
}
